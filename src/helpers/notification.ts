import { notification } from "antd";

export const openNotification = (error: string) => {
  notification.error({description: error});
};
