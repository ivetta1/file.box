import { useStores } from "../../context/rootStoreContext";
import Logo from "../Logo/Logo";
import UserBar from '../UserBar/UserBar';
import { SHeader, Button, Box } from "./Header.styled";

const Header = () => {
  const {
    user: { signOut },
  } = useStores();
  return (
    <SHeader>
      <Logo />
      <Box>
        <UserBar />
        <Button type="button" onClick={signOut}>
          Log out
        </Button>
      </Box>
    </SHeader>
  );
};

export default Header;
