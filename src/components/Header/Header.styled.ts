import styled from "styled-components";

export const SHeader = styled.header`
width: 100%;
padding: 10px 15px;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.lg};
color: ${({ theme: { colors } }) => colors.whiteColor};
 background: ${({ theme: { colors } }) => colors.blackColor};
 display: flex;
 justify-content: space-between;
 align-items: center;
  @media only screen and (min-width: 768px) {
padding: 15px 20px;
  }
  @media only screen and (min-width: 1440px) {
  padding: 20px 60px;
  }
`

export const Button = styled.button`

padding: 10px 0px;
margin-left:40px;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.md};
color: ${({ theme: { colors } }) => colors.whiteColor};
 background: ${({ theme: { colors } }) => colors.transparent};
 border: none;
 transition:${({ theme: { transition } }) => `color ${transition}`};
&:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
}
  @media only screen and (min-width: 768px) {
padding: 15px 0px;
  }
  @media only screen and (min-width: 1440px) {
  padding: 20px 0px;
  }
`
export const Box = styled.div`
display:flex;
align-items:center;
`