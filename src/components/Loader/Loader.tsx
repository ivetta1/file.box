import React from "react";
import { InfinitySpin } from "react-loader-spinner";
import { LoaderWrapper } from "./Loader.styled";
import { theme } from '../../globalStyles'

const Loader: React.FC = () => {
  return (
    <LoaderWrapper>
      <InfinitySpin visible={true} width="200" color={theme.colors.accentColor} ariaLabel="infinity-spin-loading" />
    </LoaderWrapper>
  );
};

export default Loader
