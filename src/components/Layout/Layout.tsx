import { Suspense } from "react";
import { Outlet } from "react-router-dom";
import Loader from "../Loader/Loader";
import Header from "../Header/Header";
import Sidebar from "../Sidebar/Sidebar";
import { observer } from "mobx-react-lite";
import { SMain, SSubMain } from "./Layout.styled";
import { spy } from "mobx";
import { useStores } from "../../context/rootStoreContext";
import { useLocation } from 'react-router-dom';
spy((ev) => {
  if (ev.type === "action") {
    // console.log(ev);
  }
});

const Layout: React.FC = observer(() => {
  const {
    user: { isLoading, isAuth },
  } = useStores();
  const location = useLocation();
  const queryParams = new URLSearchParams(location.search);
  const error = queryParams.get('error');

  return (
    <Suspense fallback={<Loader />}>
      {isAuth && <Header />}
      <SMain $isAuth={isAuth}>
        {isAuth && <Sidebar />}
        <SSubMain> {isLoading ? <Loader /> : <Outlet />}</SSubMain>
      </SMain>
    </Suspense>
  );
});

export default Layout;
