import styled from "styled-components";

export const SMain = styled.div`
  display: ${(props) => (props.$isAuth ? 'flex' : 'block')};
  flex-wrap: nowrap;
  align-items: stretch;
  max-width:100%;
 height:100vh;
  overflow:hidden;
`;

export const SSubMain = styled.main`
padding:15px;
max-width:85%;
width:100%;
margin:0 auto;


        @media only screen and (min-width: 768px) {
             padding:30px 20px;
        }
        @media only screen and (min-width: 1440px) {
             padding:40px 60px;
        }
`;