import AuthNav from "../AuthNav/AuthNav";
import { SSidebar } from "./Sidebar.styled";

const Sidebar = () => {
  return (
    <SSidebar>
      <AuthNav />
    </SSidebar>
  );
};

export default Sidebar;
