import styled from "styled-components";


export const SSidebar = styled.div`
display: block;
padding: 15px;
width: 15%;
background-color: ${({ theme: { colors } }) => colors.blackColor};
  @media only screen and (min-width: 600px) {
padding: 30px 20px;
width: 20%;
  }
   @media only screen and (min-width: 1440px) {
padding: 40px 60px;
  }
`
