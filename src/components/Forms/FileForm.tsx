import { useCallback, useRef, useState } from "react";
import { useDropzone } from "react-dropzone";
import { observer } from "mobx-react-lite";
import { yupResolver } from "@hookform/resolvers/yup";
import StyledCheckbox from "./fields/StyledCheckbox";
import { fileSchema } from "./schemas";
import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { SInput, ButtonSubmit, DropZone, InputFile, SButton, Box } from "./Form.styled";
import { useStores } from "../../context/rootStoreContext";

interface FileFormProps {
  onClose: () => void;
}

const FileForm: React.FC<FileFormProps> = observer(({ onClose }) => {
  const fileInputRef = useRef(null);
  const {
    file: { addFile, currentPath },
  } = useStores();
  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(fileSchema),
    defaultValues: {
      name: "",
      isPrivate: true,
      file: {},
    },
  });

  const onSubmit: SubmitHandler = async (data) => {
    const formData = new FormData();
    Object.entries(data).map((item) => {
      return formData.append(item[0], item[1]);
    });

    const currentFolder = currentPath[currentPath.length - 1]._id;
    formData.append("rootFolder", currentFolder);
    await addFile(formData);
    reset();
    onClose();
  };

  const handleFileChange = () => {
    setValue("file", fileInputRef.current.files[0], { shouldValidate: true });
    setValue("name", fileInputRef.current.files[0].name, { shouldValidate: true });
  };

  const handleButtonClick = () => {
    fileInputRef.current.click();
  };

  const onDrop = useCallback((acceptedFiles) => {
    setValue("file", acceptedFiles[0], { shouldValidate: true });
    setValue("name", acceptedFiles[0].name, { shouldValidate: true });
  }, []);

  const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop });
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <Box>
        <Controller
          name="name"
          control={control}
          render={({ field }) => {
            return <SInput {...field} status={errors.name && "error"} />;
          }}
        />

        <InputFile ref={fileInputRef} type="file" onChange={handleFileChange} />
        <SButton type="button" onClick={handleButtonClick}>
          Choose File
        </SButton>
      </Box>
      <DropZone {...getRootProps()}>
        <input {...getInputProps()} />
        {isDragActive ? <p>Drop the file here ...</p> : <p>Drag 'n' drop some files here, or click to select files</p>}
      </DropZone>
      <StyledCheckbox name="isPrivate" control={control} placeholder="Is private" />
      <ButtonSubmit type="submit">Submit</ButtonSubmit>
    </form>
  );
});

export default FileForm;
