import styled from "styled-components";
import { Input } from "antd";

export const SInput = styled.input`
  padding: 12px 20px;
  width: 70%;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  border-radius: 4px;
  background: none;
  outline: none;
  background-color: ${({ theme: { colors } }) => colors.transparent};
  border: ${({ theme: { colors } }) => `1px solid ${colors.transparent}`};
  outline: none;
  &:not(:focus) {
    background-color: ${({ theme: { colors } }) => colors.transparent};
  }
`;

export const DropZone = styled.div`
  color: ${({ theme: { colors } }) => colors.whiteColor};
  border: ${({ theme: { colors } }) => `2px dashed ${colors.whiteColor}`};
  border-radius: 4px;
  text-align: center;
  width: 100%;
  height: 150px;
  cursor: pointer;
  margin-bottom: 15px;
  margin-top: 15px;
`;
export const InputFile = styled.input`
  display: none;
`;

export const ErrorMsg = styled.p`
  color: ${({ theme: { colors } }) => colors.errorColor};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.sm};
  padding-left: 5px;
  position: absolute;
  bottom: -30px;
`;
export const SEmail = styled.p`
  margin: 0px;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  padding: 12px 20px;
  border-radius: 10px;
  width: 60%;
  border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};
`;
export const SButton = styled.button`
  background-color: ${({ theme: { colors } }) => colors.whiteColor};
  color: ${({ theme: { colors } }) => colors.blackColor};
  border: none;
  width: 30%;
  cursor: pointer;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px;
  @media only screen and (min-width: 768px) {
    padding: 12px 20px;
  }
`;

export const Box = styled.div`
  display: flex;
  justify-content: space-between;

  border-radius: 10px;
  overflow: hidden;
  border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};
`;

export const BoxInput = styled.div`
  position: relative;
`;
export const ButtonSubmit = styled.button`
  background: ${({ theme: { colors } }) =>
    `linear-gradient(180deg,${colors.whiteColor} 3.7%, ${colors.accentColor} 99.19%)`};

  color: ${({ theme: { colors } }) => colors.blackColor};
  padding: 12px 30px;
  margin-left: auto;
  display: block;
  border: none;
  transition: ${({ theme: { transition } }) => `background ${transition}`};
  border-radius: 8px;
  &:hover {
    background: ${({ theme: { colors } }) =>
      `linear-gradient(180deg,${colors.lightGreyColor} 3.7%, ${colors.accentColor} 99.19%)`};
  }
`;
export const SearchInputWrapper = styled.div`
  position: relative;
`;
export const SearchIconsWrapper = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  height: 100%;
`;

export const SearchIconWrapper = styled.button`
  height: 100%;
  border: none;
  background-color: ${({ theme: { colors } }) => colors.transparent};
`;

export const SearchInput = styled(Input)`
  background-color: ${({ theme: { colors } }) => colors.transparent};
  border-radius: 8px;
  font-weight: 400;
  color: ${({ theme: { colors } }) => colors.blackColor};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.md};

  .ant-input {
    &::placeholder {
      color: ${({ theme: { colors } }) => colors.darkGreyColor};
    }
  }

  transition: ${({ theme: { transition } }) => `background-color outline border-color color ${transition}`};
  &:hover,
  &:focus {
    background-color: ${({ theme: { colors } }) => colors.transparent};
    outline: 1px solid ${({ theme: { colors } }) => colors.blackColor};
    border-color: ${({ theme: { colors } }) => colors.blackColor};
  }

  &:not(:focus) {
    border-color: ${({ theme: { colors } }) => colors.blackColor};
    background-color: ${({ theme: { colors } }) => colors.transparent};
  }
`;
