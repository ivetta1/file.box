import React, { useEffect } from "react";
import { observer } from "mobx-react-lite";
import { yupResolver } from "@hookform/resolvers/yup";
import StyledCheckbox from "./fields/StyledCheckbox";
import { FileOutlined } from "@ant-design/icons";
import { fileEditSchema, fileForEditorSchema } from "./schemas";
import { useForm, SubmitHandler } from "react-hook-form";
import { ButtonSubmit } from "./Form.styled";
import { useStores } from "../../context/rootStoreContext";
import StyledInput from "./fields/StyledInput";
import { File } from "../../types/types";

interface FileEditFormProps {
  file: File;
  onClose: () => void;
}

const FileEditForm: React.FC<FileEditFormProps> = observer(({ file, onClose }) => {
  const {
    file: { editFile },
    user: { userData },
  } = useStores();
  const isOwner = userData._id === file.owner;
  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(isOwner ? fileEditSchema : fileForEditorSchema),
    defaultValues: {
      name: "",
      ...(isOwner && { isPrivate: true }),
    },
  });

  const onSubmit: SubmitHandler = async (data) => {
    const payload = isOwner ? data : { name: data.name };
    await editFile(file._id, payload);
    reset();
    onClose();
  };

  useEffect(() => {
    if (file) {
      setValue("name", file.name);
      setValue("isPrivate", file.isPrivate);
    } else {
      reset();
    }
  }, [file, setValue, reset]);

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <StyledInput name="name" control={control} placeholder="Name" icon={<FileOutlined />} error={errors.name} />
      {isOwner && <StyledCheckbox name="isPrivate" control={control} placeholder="Is private" />}
      <ButtonSubmit type="submit">Submit</ButtonSubmit>
    </form>
  );
});

export default FileEditForm;
