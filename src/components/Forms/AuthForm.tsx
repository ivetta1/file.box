import { useParams } from "react-router-dom";
import { useForm, SubmitHandler } from "react-hook-form";
import { MailOutlined, SafetyOutlined } from "@ant-design/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { authSchema } from "./schemas";
import { AuthFormInputs } from "../../types/types";
import StyledInput from "./fields/StyledInput";
import { SBtn } from "./AuthForm.styled";
import { observer } from "mobx-react-lite";
import { useStores } from "../../context/rootStoreContext";

export const AuthForm: React.FC = observer(() => {
  const { type } = useParams();
  const {
    user: { signUp, signIn },
  } = useStores();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<AuthFormInputs>({ resolver: yupResolver(authSchema) });

  const onSubmit: SubmitHandler<AuthFormInputs> = async (data) => {
    if (type === "signup") {
      await signUp(data);
    } else {
      await signIn(data);
    }
  };
  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <StyledInput name="email" control={control} placeholder="Email" icon={<MailOutlined />} error={errors.email} />
      <StyledInput
        type="password"
        name="password"
        control={control}
        placeholder="Password"
        icon={<SafetyOutlined />}
        error={errors.password}
      />
      <SBtn type="submit">{type === "signup" ? "Sign Up" : "Sign In"}</SBtn>
    </form>
  );
});
