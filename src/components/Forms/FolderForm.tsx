import React, { useEffect } from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { FolderOpenOutlined } from "@ant-design/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { folderSchema, folderForEditorSchema } from "./schemas";
import { Folder } from "../../types/types";
import StyledInput from "./fields/StyledInput";
import StyledCheckbox from "./fields/StyledCheckbox";

import { useStores } from "../../context/rootStoreContext";
import { observer } from "mobx-react-lite";
import { ButtonSubmit } from "./Form.styled";

interface FolderFormProps {
  folder: Folder | undefined;
  onClose: () => void;
}

const FolderForm: React.FC<FolderFormProps> = observer(({ folder, onClose }) => {
  const {
    file: { addFolder, editFolder, currentPath },
    user: { userData },
  } = useStores();

  const currentFolder = currentPath[currentPath.length - 1];

  const isOwnerCurrentFolder = currentFolder.owner === userData._id;
  const isOwner = userData._id === folder.owner;
  const isFileExist = Object.keys(folder).length > 0;

  const {
    control,
    handleSubmit,
    reset,
    setValue,
    formState: { errors },
  } = useForm<Folder>({
    resolver: yupResolver(isOwner ? folderSchema : folderForEditorSchema),
    defaultValues: {
      title: "",
      ...((isOwner || !isFileExist) && { isPrivate: true }),
    },
  });

  useEffect(() => {
    if (isFileExist) {
      setValue("title", folder.title);
      setValue("isPrivate", folder.isPrivate);
    } else {
      reset();
    }
  }, [folder, setValue, reset]);

  const onSubmit: SubmitHandler<Folder> = async (data) => {
    if (isFileExist) {
      const payload = isOwner
        ? { ...data, _id: folder._id, rootFolder: currentFolder._id }
        : { _id: folder._id, title: data.title, rootFolder: currentFolder._id };
      await editFolder(payload);
    } else {
      await addFolder({ ...data, rootFolder: currentFolder._id });
    }
    reset();
    onClose();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <StyledInput
        name="title"
        control={control}
        placeholder="Title"
        icon={<FolderOpenOutlined />}
        error={errors.title}
      />
      {(isOwner || (isOwnerCurrentFolder && !isFileExist)) && (
        <StyledCheckbox name="isPrivate" control={control} placeholder="Is private" />
      )}
      <ButtonSubmit type="submit">{isFileExist ? "Edit Folder" : "Add Folder"}</ButtonSubmit>
    </form>
  );
});

export default FolderForm;
