import styled from "styled-components";

export const SBtn = styled.button`
margin-top: 20px;
width: 100%;
padding: 16px;
border-radius: 8px;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
border: none;
color: ${({ theme: { colors } }) => colors.whiteColor};
 background:  ${({ theme: { colors } }) => `linear-gradient(180deg,${colors.whiteColor} 3.7%, ${colors.accentColor} 99.19%)`}; 
    transition:${({ theme: { transition } }) => `background ${transition}`};
 &:hover, &:focus {
    background:  ${({ theme: { colors } }) => `linear-gradient(180deg,${colors.lightGreyColor} 3.7%, ${colors.accentColor} 99.19%)`}; 
}

     @media only screen and (min-width: 768px) {
  font-size: ${({ theme: { fontSizes } }) => fontSizes.lg};
  }
  @media only screen and (min-width: 1440px) {
  font-size: ${({ theme: { fontSizes } }) => fontSizes.ml};
  }
`