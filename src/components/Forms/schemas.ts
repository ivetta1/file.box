import * as yup from "yup";

export const authSchema = yup.object().shape({
  email: yup.string().email("Invalid email").required("Email is required"),
  password: yup.string().required("Password is required").min(6, "Password must be at least 6 characters"),
});
export const changePasswordSchema = yup.object().shape({
  oldPassword: yup.string().required("Old password is required").min(6, "Old password must be at least 6 characters"),
  newPassword: yup.string().required("New password is required").min(6, "New password must be at least 6 characters"),
});

export const folderSchema = yup.object().shape({
  title: yup.string().required("Title is required"),
  isPrivate: yup.boolean().required("IsPrivate is required"),
});
export const folderForEditorSchema = yup.object().shape({
  title: yup.string().required("Title is required"),
});

export const fileSchema = yup.object().shape({
  name: yup.string().required("Name is required"),
  isPrivate: yup.boolean().required("IsPrivate is required"),
  file: yup.mixed().required("File is required"),
});

export const fileEditSchema = yup.object().shape({
  name: yup.string().required("Name is required"),
  isPrivate: yup.boolean().required("IsPrivate is required"),
});

export const fileForEditorSchema = yup.object().shape({
  name: yup.string().required("Name is required"),
});

export const searchSchema = yup.object().shape({
  keyword: yup.string().nullable().default(""),
});

export const permitSchema = yup.object().shape({
  email: yup.string().email("Invalid email").notRequired(),
  permissions: yup.array().of(
    yup.object().shape({
      email: yup.string().email("Invalid email").required("Email is required"),
      role: yup.string().oneOf(["viewer", "editor"]).required("Role is required"),
    })
  ),
});
