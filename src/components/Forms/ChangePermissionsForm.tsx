import { useForm, SubmitHandler, Controller } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { permitSchema } from "./schemas";
import { File, Folder } from "../../types/types";
import { SInput, SButton, Box, BoxInput, ErrorMsg, ButtonSubmit } from "./Form.styled";
import { useState } from "react";
import PermissionsList from "../Files/PermissionsList/PermissionsList";
import { observer } from "mobx-react-lite";
import { useStores } from "../../context/rootStoreContext";

interface ChangePermissionsFormProps {
  file: File | Folder;
  type: string;
  isOwner: boolean;
  onClose: () => void;
}

const ChangePermissionsForm: React.FC<ChangePermissionsFormProps> = observer(({ file, isOwner, type, onClose }) => {
  const {
    file: { editFilePermissions, editFolderPermissions },
  } = useStores();
  const [emailError, setEmailError] = useState();
  const { control, handleSubmit, reset, setValue, watch } = useForm<Folder>({
    resolver: yupResolver(permitSchema),
    defaultValues: {
      email: "",
      permissions: file.permissions,
    },
  });
  const permissions = watch("permissions");

  const onSubmit: SubmitHandler = async (data) => {
    if (type === "file") {
      editFilePermissions(file._id, data.permissions);
    } else {
      editFolderPermissions(file._id, data.permissions);
    }
    onClose();
    reset();
  };

  const handleGrantPermit = async (): Promise<void> => {
    try {
      const currentEmail: string = watch("email");
      const isValidEmail: boolean = await permitSchema.fields.email.validate(currentEmail);

      if (isValidEmail) {
        const foundPermit = permissions.find((permit: { email: string }) => permit.email === currentEmail);
        if (foundPermit) {
          setEmailError("Such email already exists");
        } else {
          setEmailError();
          setValue("permissions", [...permissions, { email: currentEmail, role: "viewer" }]);
          setValue("email", "");
        }
      } else {
        setEmailError("Email is empty");
      }
    } catch (error: any) {
      setEmailError(error.message);
    }
  };

  const handleChangePermit = (newPermit: { email: string; role: string }): void => {
    const newPermissions: { email: string; role: string }[] = permissions.map((permission) => {
      if (permission.email === newPermit.email) {
        permission.role = newPermit.role;
      }
      return permission;
    });
    setValue("permissions", newPermissions);
  };

  const handleRemovePermit = (permit: { email: string }): void => {
    const newPermissions: { email: string; role: string }[] = permissions.filter(
      (permission) => permission.email !== permit.email
    );
    setValue("permissions", newPermissions);
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      {isOwner && (
        <BoxInput>
          <Box>
            <Controller
              name="email"
              control={control}
              render={({ field }) => {
                return <SInput {...field} placeholder="Email" status={emailError && "error"} />;
              }}
            />
            <SButton type="button" onClick={handleGrantPermit}>
              Grant permit
            </SButton>
          </Box>
          {emailError && <ErrorMsg>{emailError}</ErrorMsg>}
        </BoxInput>
      )}
      <PermissionsList
        permissions={permissions}
        isOwner={isOwner}
        onChangePermit={handleChangePermit}
        onRemovePermit={handleRemovePermit}
      />
      {isOwner && <ButtonSubmit type="submit">Submit</ButtonSubmit>}
    </form>
  );
});

export default ChangePermissionsForm;
