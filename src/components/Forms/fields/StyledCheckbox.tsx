import { Controller } from "react-hook-form";
import { Label, Input, Box, Title } from "./StyledCheckbox.styled";

const StyledCheckbox = ({ name, control, placeholder }) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={false} 
      render={({ field }) => {
        return (
          <Box>
            <Input
              type="checkbox"
              id={name} 
              checked={field.value}
              onChange={(e) => field.onChange(e.target.checked)}
            />
            <Label htmlFor={name}></Label>
            <Title>{placeholder}</Title>
          </Box>
        );
      }}
    />
  );
};

export default StyledCheckbox;
