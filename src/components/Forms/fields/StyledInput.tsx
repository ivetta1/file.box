import { Controller } from "react-hook-form";
import { SInput, SError, SSuccess, SInputPassword } from "./StyledInput.styled";

const StyledInput = ({ name, control, placeholder, icon, error, type = "text" }) => {
  return (
    <>
      <Controller
        name={name}
        control={control}
        render={({ field }) => {
          return type === "password" ? (
            <SInputPassword {...field} placeholder={placeholder} prefix={icon} status={error && "error"} />
          ) : (
            <SInput {...field} placeholder={placeholder} prefix={icon} status={error && "error"} />
          );
        }}
      />
      {error ? <SError>{error.message}</SError> : <SSuccess>Success</SSuccess>}
    </>
  );
};

export default StyledInput;
