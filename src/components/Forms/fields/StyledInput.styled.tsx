import styled from "styled-components";
import { Input } from "antd";

export const SInput = styled(Input)`
  background-color: ${({ theme: { colors } }) => colors.transparent};
  padding: 16px;
  border-radius: 8px;
  font-weight: 400;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.md};

  .ant-input {
    &::placeholder {
      color: ${({ theme: { colors } }) => colors.whiteColor};
    }
  }

  .anticon svg {
    width: 20px;
    height: 20px;
    margin-right: 10px;
  }
  transition: ${({ theme: { transition } }) => `background-color outline border-color color ${transition}`};
  &:hover,
  &:focus {
    background-color: ${({ theme: { colors } }) => colors.transparent};
    outline: 1px solid ${({ theme: { colors } }) => colors.whiteColor};
    border-color: ${({ theme: { colors } }) => colors.whiteColor};
  }

  &:not(:focus) {
    border-color: ${({ theme: { colors } }) => colors.whiteColor};
    color: ${({ theme: { colors } }) => colors.whiteColor};
    background-color: ${({ theme: { colors } }) => colors.transparent};
  }
  &&.ant-input-status-error {
    border-color: ${({ theme: { colors } }) => colors.errorColor};
    color: ${({ theme: { colors } }) => colors.errorColor};
    background-color: ${({ theme: { colors } }) => colors.transparent};
    &:hover,
    &:focus {
      background-color: ${({ theme: { colors } }) => colors.transparent};
      outline: 1px solid ${({ theme: { colors } }) => colors.errorColor};
      border-color: ${({ theme: { colors } }) => colors.errorColor};
    }
    &:not(:focus) {
      background-color: ${({ theme: { colors } }) => colors.transparent};
    }
  }
`;

export const SInputPassword = styled(SInput).attrs({ type: "password" })``;

export const SError = styled.span`
  color: ${({ theme: { colors } }) => colors.errorColor};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xs};
  padding: 4px 0;
  display: block;
`;
export const SSuccess = styled.span`
  color: ${({ theme: { colors } }) => colors.transparent};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xs};
  padding: 4px 0;
  display: block;
`;
