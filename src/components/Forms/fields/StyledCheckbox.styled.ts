import styled from "styled-components";
import checkIcon from './check.svg';

export const Box = styled.div`
  display: inline-block;
  display:flex;
  color: ${({ theme: { colors } }) => colors.whiteColor};
`;
export const Label = styled.label`

  cursor: pointer;


  position: relative;

`;
export const Title = styled.span`
display:block;
margin-left:10px;
`
export const Input = styled.input.attrs({ type: 'checkbox' })`
  appearance: none;
  width: 25px;
  height: 25px;
    cursor: pointer;
 border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};
border-radius: 6px;
  transform: translateY(-0.075em);

  &:before {
    content: '';
    display: inline-block;
    position: absolute;
    left: -10px;
    width: 16px;
    height: 16px;
    border-radius: 4px;
  }

  &:checked + ${Label}::before {
    content: url(${checkIcon});
    display: inline-block;
    position: absolute;
    left: -20px;
    top:2px;
    width: 16px;
    height: 16px; 
    border-radius: 4px;
  }
`;
