import React, { useEffect } from "react";
import { SearchOutlined, CloseOutlined } from "@ant-design/icons";
import { Controller, useForm, SubmitHandler } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { observer } from "mobx-react-lite";
import { Search } from "../../types/types";
import { useStores } from "../../context/rootStoreContext";
import { searchSchema } from "./schemas";
import { SearchInputWrapper, SearchInput, SearchIconWrapper, SearchIconsWrapper } from "./Form.styled";

const SearchForm: React.FC = observer(() => {
  const {
    file: { searchFiles, getFiles, search },
  } = useStores();

  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm<Search>({
    resolver: yupResolver(searchSchema),
    defaultValues: {
      keyword: "",
    },
  });

  const onSubmit: SubmitHandler<Search> = async (data) => {
    await searchFiles(data);
  };

  const handleClearInput = () => {
    reset();
    getFiles();
  };

  const handleFormSubmit = () => {
    handleSubmit(onSubmit)();
  };
    
     const handleKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
       if (e.key === "Enter") {
         e.preventDefault();
         handleFormSubmit();
       }
     };


  useEffect(() => {
    if (!search && watch("keyword")) {
      reset();
    }
  }, [search]);

  return (
    <form onSubmit={handleFormSubmit}>
      <Controller
        name="keyword"
        control={control}
        render={({ field }) => (
          <SearchInputWrapper>
            <SearchInput
              placeholder="Search"
              {...field}
              status={errors.keyword && "error"}
              onKeyPress={handleKeyPress}
            />
            <SearchIconsWrapper>
              {field.value && (
                <SearchIconWrapper type="button" onClick={handleClearInput}>
                  <CloseOutlined />
                </SearchIconWrapper>
              )}
              <SearchIconWrapper type="button" onClick={handleFormSubmit}>
                <SearchOutlined />
              </SearchIconWrapper>
            </SearchIconsWrapper>
          </SearchInputWrapper>
        )}
      />
    </form>
  );
});

export default SearchForm;
