import React from "react";
import { useForm, SubmitHandler } from "react-hook-form";
import { SafetyOutlined } from "@ant-design/icons";
import { yupResolver } from "@hookform/resolvers/yup";
import { changePasswordSchema } from "./schemas";
import { ChangePasswordInputs } from "../../types/types";
import StyledInput from "./fields/StyledInput";
import { useStores } from "../../context/rootStoreContext";
import { observer } from "mobx-react-lite";
import { ButtonSubmit } from "./Form.styled";

interface ChangePasswordFormProps {
  onClose: () => void;
}

const ChangePasswordForm: React.FC<ChangePasswordFormProps> = observer(({ onClose }) => {
  const {
    user: { changePassword },
  } = useStores();
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm<ChangePasswordInputs>({ resolver: yupResolver(changePasswordSchema) });

  const onSubmit: SubmitHandler<ChangePasswordInputs> = async (data) => {
    await changePassword(data);
    onClose();
  };

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
      <StyledInput
        type="password"
        name="oldPassword"
        control={control}
        placeholder="Old password"
        icon={<SafetyOutlined />}
        error={errors.oldPassword}
      />
      <StyledInput
        type="password"
        name="newPassword"
        control={control}
        placeholder="New password"
        icon={<SafetyOutlined />}
        error={errors.newPassword}
      />
      <ButtonSubmit type="submit">Submit</ButtonSubmit>
    </form>
  );
});

export default ChangePasswordForm;
