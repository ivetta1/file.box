import React from "react";
import { observer } from "mobx-react-lite";
import { SModal, Question, Buttons, Button, SecondaryButton } from "./Modal.styled";
import { useStores } from "../../context/rootStoreContext";
import { Folder, ModalProps } from "../../types/types";

const FolderDeleteModal: React.FC<ModalProps> = observer(({ isOpen, onClose, folder }) => {
  const {
    file: { deleteFolder },
  } = useStores();

  const handleFolderDelete = async () => {
    await deleteFolder(folder._id);
    onClose();
  };

  const handleCancel = () => {
    onClose();
  };

  return (
    <SModal title="Delete folder" open={isOpen} onOk={handleFolderDelete} onCancel={handleCancel}>
      <Question>{`Are you sure you want to delete a folder ${
        (folder as Folder)?.folders?.length > 0 || (folder as Folder)?.files?.length > 0
          ? `${folder.title} with all files`
          : `${folder.title}`
      }?`}</Question>
      <Buttons>
        <SecondaryButton type="button" onClick={handleCancel}>
          Cancel
        </SecondaryButton>
        <Button type="button" onClick={handleFolderDelete}>
          Delete
        </Button>
      </Buttons>
    </SModal>
  );
});

export default FolderDeleteModal;
