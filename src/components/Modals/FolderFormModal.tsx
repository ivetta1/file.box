import { SModal } from "./Modal.styled";
import FolderForm from "../Forms/FolderForm";
import { Folder, ModalProps } from "../../types/types";

const FolderFormModal: React.FC<ModalProps> = ({ isOpen, onClose, folder }) => {
  return (
    <SModal
      title={`${Object.keys(folder as Folder).length ? "Edit" : "Add"} folder`}
      open={isOpen}
      onOk={onClose}
      onCancel={onClose}
    >
      <FolderForm folder={folder as Folder} onClose={onClose} />
    </SModal>
  );
};

export default FolderFormModal;
