import React from "react";
import { observer } from "mobx-react-lite";
import { SModal, Question, Buttons, Button, SecondaryButton } from "./Modal.styled";
import { useStores } from "../../context/rootStoreContext";
import { ModalProps } from "../../types/types";

const FileDeleteModal: React.FC<ModalProps> = observer(({ isOpen, onClose, file }) => {
  const {
    file: { deleteFile },
  } = useStores();

  const handleFileDelete = async () => {
    await deleteFile(file._id);
    onClose();
  };

  return (
    <SModal title="Delete file" open={isOpen} onOk={onClose} onCancel={onClose}>
      <Question>{`Are you sure you want to delete the file ${file.name}?`}</Question>
      <Buttons>
        <SecondaryButton type="button" onClick={onClose}>
          Cancel
        </SecondaryButton>
        <Button type="button" onClick={handleFileDelete}>
          Delete
        </Button>
      </Buttons>
    </SModal>
  );
});

export default FileDeleteModal;
