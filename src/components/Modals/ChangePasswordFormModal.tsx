import { SModal } from "./Modal.styled";
import { ModalProps } from "../../types/types";
import ChangePasswordForm from "../Forms/ChangePasswordForm";

const ChangePasswordFormModal: React.FC<ModalProps> = ({ isOpen, onClose }) => {
  return (
    <SModal title="Change user password" open={isOpen} onOk={onClose} onCancel={onClose}>
      <ChangePasswordForm onClose={onClose} />
    </SModal>
  );
};

export default ChangePasswordFormModal;
