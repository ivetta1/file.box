import React from "react";
import { SModal } from "./Modal.styled";
import FileForm from "../Forms/FileForm";
import FileEditForm from "../Forms/FileEditForm";
import { File, ModalProps } from "../../types/types";

const FileFormModal: React.FC<ModalProps> = ({ isOpen, onClose, file }) => {
  return (
    <SModal
      title={`${Object.keys(file as File).length ? "Edit" : "Add"} file`}
      open={isOpen}
      onOk={onClose}
      onCancel={onClose}
    >
      {Object.keys(file as File).length ? (
        <FileEditForm file={file as File} onClose={onClose} />
      ) : (
        <FileForm onClose={onClose} />
      )}
    </SModal>
  );
};

export default FileFormModal;
