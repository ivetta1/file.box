import styled from "styled-components";
import { Modal } from "antd";

export const SModal = styled(Modal)`
  .ant-modal-content {
        padding: 30px;
    background-color: ${({ theme: { colors } }) => colors.blackColor};
  }

  .ant-modal-header {
margin-bottom:20px
  }

  .ant-modal-title {
      font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
color:  ${({ theme: { colors } }) => colors.whiteColor};
  background-color:  ${({ theme: { colors } }) => colors.blackColor};
  }

  .ant-modal-body {
     background-color:  ${({ theme: { colors } }) => colors.blackColor};
  }

  .ant-modal-footer {
     display: none;
  }
   .ant-modal-close-x {
    color: ${({ theme: { colors } }) => colors.whiteColor};
    font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
  }
`;
export const Question = styled.p`
  color:  ${({ theme: { colors } }) => colors.whiteColor};
   font-size: ${({ theme: { fontSizes } }) => fontSizes.lg};
   
`
export const Buttons = styled.div`
display: flex;
justify-content: flex-end;
align-items: center;
`


export const SecondaryButton = styled.button`
padding:7px;
width: 100px;
  border-radius: 8px;
  border: none;
    background-color:  ${({ theme: { colors } }) => colors.whiteColor};
  color:  ${({ theme: { colors } }) => colors.blackColor};
   margin-right: 10px;
       transition:${({ theme: { transition } }) => `background ${transition}`};
   
 &:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
 }
   
`;

export const Button = styled(SecondaryButton)`
  background:  ${({ theme: { colors } }) => `linear-gradient(180deg,${colors.whiteColor} 3.7%, ${colors.accentColor} 99.19%)`}; 

`;
