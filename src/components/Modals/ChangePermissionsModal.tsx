import React from "react";
import { SModal } from "./Modal.styled";
import ChangePermissionsForm from "../Forms/ChangePermissionsForm";
import { File, Folder } from "../../types/types";
import { observer } from "mobx-react-lite";
import { useStores } from "../../context/rootStoreContext";

export interface ChangePermissionsModalProps {
  isOpen: boolean;
  onClose: () => void;
  file: Folder | File;
}
const ChangePermissionsModal: React.FC<ChangePermissionsModalProps> = observer(({ isOpen, onClose, file }) => {
  const {
    user: { userData },
  } = useStores();

  const isOwner = file.owner === userData._id;
  const type = file.name ? "file" : "folder";

  return (
    <SModal
      title={
        !isOwner
          ? `Permissions for ${type === "file" ? file.name : file.title}`
          : `Change permissions for ${type === "file" ? file.name : file.title}`
      }
      open={isOpen}
      onOk={onClose}
      onCancel={onClose}
    >
      <ChangePermissionsForm onClose={onClose} isOwner={isOwner} file={file} type={type} />
    </SModal>
  );
});

export default ChangePermissionsModal;
