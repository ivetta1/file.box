import React from "react";
import {
  FileOutlined,
  EditOutlined,
  DeleteOutlined,
  LockOutlined,
  TeamOutlined,
  ApartmentOutlined,
  UnlockOutlined,
  CloudDownloadOutlined,
} from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import { Card, Icon, Title, Subtitle, Buttons, Button, Status, ButtonDownload } from "./FileItem.styled";
import { File, Folder } from "../../../types/types";
import { useStores } from "../../../context/rootStoreContext";

interface FileItemProps {
  file: File;
  setFileForEdit: (file: File) => void;
  setFileForDelete: (file: File) => void;
  setChangeFilePermissions: (file: File | Folder) => void;
}

const FileItem: React.FC<FileItemProps> = React.memo(
  observer(({ file, setFileForEdit, setFileForDelete, setChangeFilePermissions }) => {
    const {
      user: { userData },
    } = useStores();

    const isViewer =
      file.owner !== userData._id &&
      file.permissions.find((permit) => permit.email === userData.email).role === "viewer"
        ? true
        : false;

    const isEditor =
      file.owner !== userData._id &&
      file.permissions.find((permit) => permit.email === userData.email)?.role === "editor"
        ? true
        : false;
    const isOwner = file.owner === userData._id;
    return (
      <Card>
        <Status>
          {!isViewer ? (
            <>
              <Button type="button" onClick={() => setFileForEdit(file)}>
                {!file.isPrivate ? isEditor ? <ApartmentOutlined /> : <UnlockOutlined /> : <LockOutlined />}
              </Button>
              {!file.isPrivate && (
                <Button type="button" onClick={() => setChangeFilePermissions(file)}>
                  <TeamOutlined />
                </Button>
              )}
            </>
          ) : (
            <Button type="button" onClick={() => setChangeFilePermissions(file)}>
              <ApartmentOutlined />
            </Button>
          )}
        </Status>
        <Buttons>
          <ButtonDownload href={file.url} download aria-label="Download file" target="_blank" rel="noopener noreferrer">
            <CloudDownloadOutlined />
          </ButtonDownload>
          {!isViewer && (
            <>
              <Button type="button" onClick={() => setFileForEdit(file)}>
                <EditOutlined />
              </Button>
              {isOwner && (
                <Button type="button" onClick={() => setFileForDelete(file)}>
                  <DeleteOutlined />
                </Button>
              )}
            </>
          )}
        </Buttons>
        <Icon>
          <FileOutlined />
        </Icon>
        <Title>{file.name}</Title>
        <Subtitle>{Math.round(file.size / 1000)}Kb</Subtitle>
      </Card>
    );
  })
);
export default FileItem;
