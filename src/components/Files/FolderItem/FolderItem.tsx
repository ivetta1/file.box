import React from "react";
import {
  FolderOpenOutlined,
  ApartmentOutlined,
  TeamOutlined,
  EditOutlined,
  DeleteOutlined,
  LockOutlined,
  UnlockOutlined,
} from "@ant-design/icons";
import { observer } from "mobx-react-lite";
import { Folder } from "../../../types/types";
import { Card, Icon, Title, Subtitle, Buttons, Button, Status } from "./FolderItem.styled";
import { useStores } from "../../../context/rootStoreContext";

interface Props {
  folder: Folder;
  setFolderForEdit: (folder: Folder) => void;
  setFolderForDelete: (folder: Folder) => void;
  setChangeFilePermissions: (folder: Folder) => void;
}

const FolderItem: React.FC<Props> = React.memo(
  observer(({ folder, setFolderForEdit, setFolderForDelete, setChangeFilePermissions }) => {
    const {
      file: { getFiles },
      user: { userData },
    } = useStores();

    const handleEditClick = () => {
      setFolderForEdit(folder);
    };

    const handleDeleteClick = () => {
      setFolderForDelete(folder);
    };

    const handleDoubleClickByCard = () => {
      getFiles(folder._id);
    };

      const isViewer =
        folder.owner !== userData._id &&
        folder.permissions.find((permit) => permit.email === userData.email).role === "viewer"
          ? true
          : false;
    const isEditor =
      folder.owner !== userData._id &&
      folder.permissions.find((permit) => permit.email === userData.email)?.role === "editor"
        ? true
        : false;
   
    const isOwner = folder.owner === userData._id;

    return (
      <Card onDoubleClick={handleDoubleClickByCard}>
        <Status>
          {!isViewer ? (
            <>
              <Button type="button" onClick={handleEditClick}>
                {!folder.isPrivate ? isEditor ? <ApartmentOutlined /> : <UnlockOutlined /> : <LockOutlined />}
              </Button>
              {!folder.isPrivate && (
                <Button type="button" onClick={() => setChangeFilePermissions(folder)}>
                  <TeamOutlined />
                </Button>
              )}
            </>
          ) : (
            <Button type="button" onClick={() => setChangeFilePermissions(folder)}>
              <ApartmentOutlined />
            </Button>
          )}
        </Status>
        <Buttons>
          {!isViewer && (
            <>
              <Button type="button" onClick={handleEditClick}>
                <EditOutlined />
              </Button>
              {isOwner && <Button type="button" onClick={handleDeleteClick}>
                <DeleteOutlined />
              </Button>}
            </>
          )}
        </Buttons>
        <Icon>
          <FolderOpenOutlined />
        </Icon>
        <Title>{folder.title}</Title>
        <Subtitle>{`${folder.folders.length + folder.files.length} items`}</Subtitle>
      </Card>
    );
  })
);

export default FolderItem;
