import styled from "styled-components";

export const Card = styled.li`
  min-width: 280px;
  position: relative;
  height: 150px;
  min-height: 150px;
  width: 30%;
  border-radius: 8px;
  padding: 30px 14px 14px;
  display: flex;
  flex-direction: column;
  align-items: center;
  cursor: pointer;
  transition: ${({ theme: { transition } }) => `border ${transition}`};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.lg};
  border: ${({ theme: { colors } }) => `2px solid ${colors.blackColor}`};
  
  &:hover {
    border: ${({ theme: { colors } }) => `2px solid ${colors.whiteColor}`};
  }

  &:not(:last-of-type) {
    margin-right: 30px;
    margin-bottom: 30px;
  }

  @media only screen and (min-width: 768px) {
    height: 200px;
    min-height: 200px;
  }
`;

export const Icon = styled.span`
  display: block;
  margin-bottom: 5px;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.big};
`;

export const Title = styled.span`
  font-weight: 500;
  margin-bottom: 15px;
  text-align: center;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.lg};
  width: 80%;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const Subtitle = styled.span`
  color: ${({ theme: { colors } }) => colors.blackColor};
  font-size: ${({ theme: { fontSizes } }) => fontSizes.md};
  opacity: 0.5;
`;
export const Status = styled.div`
  position: absolute;
  top: 10px;
  left: 10px;`
export const Buttons = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;
`;

export const Button = styled.button`
  margin: 2px;
  border: none;
    padding:5px;
  background-color: ${({ theme: { colors } }) => colors.transparent};
  &:hover,
  &:focus {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
`;
