
import { List } from './PermissionsList.styled';
import PermissionsItem from '../PermissionsItem/PermissionsItem';
import { Permissions, Permit } from '../../../types/types';

interface PermissionsListProps {
  permissions: Permissions;
  onChangePermit: (permit: Permit) => void;
  onRemovePermit: (permit: Permit) => void;
  isOwner: boolean
}

const PermissionsList: React.FC<PermissionsListProps> = ({ permissions, onChangePermit, isOwner, onRemovePermit }) => {
  return (
    <List>
      {permissions.map((permit) => (
        <PermissionsItem
          permit={permit}
          key={permit.email}
          isOwner={isOwner}
          onChangePermit={onChangePermit}
          onRemovePermit={onRemovePermit}
        />
      ))}
    </List>
  );
};

export default PermissionsList