import { observer } from "mobx-react-lite";
import { useStores } from "../../../context/rootStoreContext";
import { Container, Title, List, ListWrapper } from "../List.styled";
import FileItem from "../FileItem/FileItem";
import { File, Folder } from "../../../types/types";

interface FilesListProps {
  setFileForEdit: (file: File) => void;
  setFileForDelete: (file: File) => void;
  setChangeFilePermissions: (file: File | Folder) => void;
}

const FilesList: React.FC<FilesListProps> = observer(
  ({ setFileForEdit, setFileForDelete, setChangeFilePermissions }) => {
    const {
      file: {
        folderData: { files=[], permissiveFiles=[] },
      },
    } = useStores();

    return (
      <Container>
        <Title>Files</Title>
        <ListWrapper>
          {[...files, ...permissiveFiles].length > 0 ? (
            <List>
              {[...files, ...permissiveFiles].map((file: File) => (
                <FileItem
                  key={file._id}
                  file={file}
                  setFileForEdit={setFileForEdit}
                  setFileForDelete={setFileForDelete}
                  setChangeFilePermissions={setChangeFilePermissions}
                />
              ))}
            </List>
          ) : (
            <p>No folders</p>
          )}
        </ListWrapper>
      </Container>
    );
  }
);

export default FilesList;
