import React, { memo } from "react";
import { observer } from "mobx-react-lite";
import { useStores } from "../../../context/rootStoreContext";
import FolderItem from "../FolderItem/FolderItem";
import { Container, Title, List, ListWrapper } from "../List.styled";
import { Folder } from "../../../types/types";

interface FolderListProps {
  setFolderForEdit: (folder: any) => void;
  setFolderForDelete: (folder: any) => void;
  setChangeFilePermissions: (folder: any) => void;
}

const FoldersList: React.FC<FolderListProps> = observer(
  ({ setFolderForEdit, setFolderForDelete, setChangeFilePermissions }) => {
    const {
      file: {
        folderData: { folders = [], permissiveFolders = [] },
      },
    } = useStores();

    return (
      <Container>
        <Title>Folders</Title>
        <ListWrapper>
          {[...folders, ...permissiveFolders].length > 0 ? (
            <List>
              {[...folders, ...permissiveFolders].map((folder: Folder) => (
                <FolderItem
                  key={folder._id}
                  folder={folder}
                  setFolderForEdit={setFolderForEdit}
                  setFolderForDelete={setFolderForDelete}
                  setChangeFilePermissions={setChangeFilePermissions}
                />
              ))}
            </List>
          ) : (
            <p>No folders</p>
          )}
        </ListWrapper>
      </Container>
    );
  }
);

export default memo(FoldersList);
