import styled from "styled-components";

export const Container = styled.div`
  margin-bottom: 50px;
`;

export const Title = styled.h3`
  margin-bottom: 30px;
  font-weight: 500;
  text-decoration: underline;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
`;

export const ListWrapper = styled.div`
  height: 200px;
  overflow-x: auto;
  scrollbar-width: thin;
  scrollbar-color: ${({ theme }) => theme.colors.accentColor} ${({ theme }) => theme.colors.blackColor};
  
  &::-webkit-scrollbar {
    width: 10px;
  }

  &::-webkit-scrollbar-track {
    background: ${({ theme }) => theme.colors.accentColor};
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb {
    background: ${({ theme }) => theme.colors.whiteColor};
    border-radius: 10px;
  }
`;

export const List = styled.ul`
  display: flex;
  flex-wrap: wrap;
`;
