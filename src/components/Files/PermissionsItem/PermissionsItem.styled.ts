import styled from "styled-components";

export const Item = styled.li`
display: flex;
justify-content: space-between;
align-items: baseline;
margin-top: 10px;
`
export const SEmail = styled.p`
margin: 0px;
  color: ${({ theme: { colors } }) => colors.whiteColor};
padding: 12px 20px;
   border-radius: 10px;
   width:60%;
     border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};

`
export const DeleteBtn = styled.button`
border: none; color: ${({ theme: { colors } }) => colors.whiteColor};
 background-color: ${({ theme: { colors } }) => colors.transparent};
`
export const SelectStyles = {
    control: () => ({
        display: "flex",
        borderRadius: "10px",
        border: "1px solid #fff",
        padding: "9px",
        width: '150px'
    }),
    singleValue: (provided: any) => ({
        ...provided,
        color: '#fff',
        cursor: 'pointer'
    }),
    indicatorSeparator: () => ({
        display: 'none',
    }),
    dropdownIndicator: () => ({
        display: 'none',
    }),
    option: () => ({
        cursor: "pointer",
        "&:hover": {
            color: "#5b6cff",
        },
    }),
    menu: () => ({
        padding: "6px 14px",
        backgroundColor: "#161616",
        color: "#fff",
        border: "1px solid #fff",
        marginTop: "4px",
        borderRadius: "10px",
        position: "absolute",
        width: "100%",
        zIndex: '1000'
    }),
};