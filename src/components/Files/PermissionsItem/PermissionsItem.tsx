import Select from "react-select";
import { CloseOutlined } from "@ant-design/icons";
import { DeleteBtn, Item, SEmail, SelectStyles } from "./PermissionsItem.styled";
import { Permit } from "../../../types/types";

interface PermissionsItemProps {
  permit: Permit;
  isOwner: boolean;
  onChangePermit: (permit: Permit) => void;
  onRemovePermit: (permit: Permit) => void;
}

const PermissionsItem: React.FC<PermissionsItemProps> = ({ permit, onChangePermit, isOwner, onRemovePermit }) => {
  const handleChange = (selectedOption) => {
    onChangePermit({ ...permit, role: selectedOption.key });
  };
  const handleRemove = () => {
    onRemovePermit(permit);
  };

  const roles = [
    { label: "Viewer", key: "viewer" },
    { label: "Editor", key: "editor" },
  ];

  const currentRole = roles.find((role) => role.key === permit.role);

  return (
    <Item>
      <SEmail>{permit.email}</SEmail>
      <Select
        isDisabled={!isOwner}
        isSearchable={false}
        options={roles}
        styles={SelectStyles}
        onChange={handleChange}
        value={currentRole}
      />
      {isOwner && (
        <DeleteBtn type="button" onClick={handleRemove}>
          <CloseOutlined />
        </DeleteBtn>
      )}
    </Item>
  );
};

export default PermissionsItem;
