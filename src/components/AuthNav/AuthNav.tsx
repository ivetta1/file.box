import React from "react";
import { FolderOpenOutlined } from "@ant-design/icons";
import { SLink, SItem, SLinkName } from "./AuthNav.styled";

const AuthNav: React.FC = () => {
  return (
    <nav>
      <ul>
        <SItem>
          <SLink
            to="/files"
            aria-label="Link to Files page"
            aria-current={(match, location) => (location.pathname === "/files" ? "page" : undefined)}
          >
            <FolderOpenOutlined />
            <SLinkName>Files</SLinkName>
          </SLink>
        </SItem>
      </ul>
    </nav>
  );
};

export default AuthNav;
