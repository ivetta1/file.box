import styled from "styled-components";
import { NavLink } from "react-router-dom";

export const SItem = styled.li`
  margin-bottom: 20px;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.big};

  @media only screen and (min-width: 768px) {
    margin-bottom: 40px;
  }
`;

export const SLink = styled(NavLink)`
  display: flex;
  align-items: center;
  justify-content: center;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  &.active,
  &:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
  @media only screen and (min-width: 768px) {
    justify-content: flex-start;
  }
`;

export const SLinkName = styled.span`
  display: none;
  @media only screen and (min-width: 768px) {
    display: block;
    margin-left: 10px;
    font-size: ${({ theme: { fontSizes } }) => fontSizes.sm};
  }
`;
