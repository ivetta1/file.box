import { SLogo } from './Logo.styled';

const Logo = () => {
  return <SLogo>File.box</SLogo>;
}

export default Logo