import { useState } from "react";
import { observer } from "mobx-react-lite";
import type { MenuProps } from "antd";
import { Dropdown, Space } from "antd";
import { useStores } from "../../context/rootStoreContext";
import { SUserBar, Avatar, BtnMenu } from "./UserBar.styled";
import ChangePasswordFormModal from "../Modals/ChangePasswordFormModal";


const UserBar = observer(() => {
  const {
    user: { userData },
  } = useStores();
  const [isOpenModalChangePassword, setIsOpenModalChangePassword] = useState(false);
  const items: MenuProps["items"] = [
    {
      key: "1",
      label: (
        <BtnMenu type="button" onClick={() => setIsOpenModalChangePassword(true)}>
          Change password
        </BtnMenu>
      ),
    },
  ];
  return (
    <>
      <Dropdown menu={{ items }} placement="bottomLeft" overlayClassName="custom-dropdown-menu">
        <SUserBar>
          <Avatar src={userData.avatar} />
          <span>{userData.name}</span>
        </SUserBar>
      </Dropdown>

      <Space direction="vertical">
        <Space wrap></Space>
      </Space>
      {isOpenModalChangePassword && (
        <ChangePasswordFormModal
          isOpen={isOpenModalChangePassword}
          onClose={() => setIsOpenModalChangePassword(false)}
        />
      )}
    </>
  );
});

export default UserBar;
