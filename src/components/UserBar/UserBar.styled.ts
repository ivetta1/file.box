import styled from "styled-components";

export const SUserBar = styled.button`
  display: flex;
  align-items: center;
  padding: 2px 8px 2px 3px;
  border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};
  border-radius: 30px;
  background: ${({ theme: { colors } }) => colors.transparent};
  color: ${({ theme: { colors } }) => colors.whiteColor};
  @media only screen and (min-width: 768px) {
    padding: 4px 16px 4px 6px;
  }
`;

export const Avatar = styled.img`
  width: 25px;
  height: 25px;
  margin-right: 8px;
  background: ${({ theme: { colors } }) => colors.whiteColor};
  border-radius: 100%;
  @media only screen and (min-width: 768px) {
    width: 50px;
    height: 50px;
    margin-right: 15px;
  }
`;
export const BtnMenu = styled.button`
  border: none;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  background: ${({ theme: { colors } }) => colors.transparent};
  &:hover,
  &:focus {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
`;
