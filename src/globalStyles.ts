import { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
body {
  max-height: 100vh;
  height: 100vh;
  overflow: hidden;
  margin: 0;
  font-family: 'Poppins', sans-serif;
  font-weight: 400;
  background-color: transparent;
  background:  ${({ theme: { colors } }) =>
    `linear-gradient(180deg,${colors.whiteColor} 13.7%, ${colors.accentColor} 92.19%)`}; 
  font-synthesis: none;
  text-rendering: optimizeLegibility;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
//   *,
// *::after,
// *::before {
//   outline: 2px solid red;
// }

 input:-webkit-autofill,
  input:-webkit-autofill:hover,
  input:-webkit-autofill:focus,
  input:-internal-autofill-selected {
    border:none;
    -webkit-box-shadow: 0 0 0 30px ${({ theme: { colors } }) => colors.blackColor} inset !important;
    -webkit-text-fill-color: ${({ theme: { colors } }) => colors.whiteColor} !important;
  }

  input:-webkit-autofill {
  border:none;
    background-color: ${({ theme: { colors } }) => colors.blackColor} !important;
  }
  
   @media only screen and (min-width: 320px) {
    font-size: 10px;
  }
   @media only screen and (min-width: 768px) {
    font-size: 14px;
  }
   @media only screen and (min-width: 1440px) {
    font-size: 18px;
  }
}

h1,
h2,
h3,
h4,
h5,
h6,
p {
  margin-top: 0;
}
a {
  text-decoration: none;
  cursor:pointer;
}
ul, li {
  list-style: none;
  padding: 0;
  margin: 0;
}
img {
  display: block;
}
button {
  cursor: pointer;
}
.custom-dropdown-menu .ant-dropdown-menu  {
    background-color: ${({ theme: { colors } }) => colors.blackColor};
  border: ${({ theme: { colors } }) => `1px solid ${colors.whiteColor}`};
   
  }
`;

const fonts = {
  main: {
    regular: "Poppins",
  },
  secondary: {},
};

const media = {
  phone: "(min-width: 320px)",
  tablet: "(min-width: 768px)",
  desktop: "(min-width: 1280px)",
};

const fontSizes = {
  xs: "0.75em",
  sm: "0.875em",
  md: "1em",
  ml: "1.125em",
  lg: "1.25em",
  xl: "1.5em",
  xxl: "1.75em",
  xxxl: "2em",
  big: "2.25em",
  huge: "3em",
  gigantic: "4.25em",
};

const transition = "250ms cubic-bezier(0.4, 0, 0.2, 1)";

export const colors = {
  whiteColor: " #fff",
  blackColor: " #161616",
  accentColor: "#5b6cff",
  lightGreyColor: "rgba(255, 255, 255,0.8)",
  greyColor: "rgba(22, 22, 22,0.5)",
  errorColor: "red",
  transparent: "transparent",
};

export const theme = {
  colors,
  media,
  fonts,
  fontSizes,
  transition,
};
