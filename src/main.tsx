import ReactDOM from "react-dom/client";
import { BrowserRouter } from "react-router-dom";
import { ThemeProvider } from "styled-components";
import App from "./App.tsx";
import { GlobalStyles, theme } from "./globalStyles.ts";
import { RootStoreContext } from "./context/rootStoreContext.ts";
import RootStore from "./stores/rootStore.tsx";

ReactDOM.createRoot(document.getElementById("root")!).render(

    <BrowserRouter basename="/">
      <RootStoreContext.Provider value={new RootStore()}>
        <ThemeProvider theme={theme}>
          <App />
          <GlobalStyles />
        </ThemeProvider>
      </RootStoreContext.Provider>
    </BrowserRouter>
);
