import { makeAutoObservable, runInAction } from "mobx";
import { api } from "../services/axios";
import UserStore from "./userStore";
import { CreateFile, CreateFolder, Folder, Folders, Permissions, User } from "../types/types";
import {openNotification} from '../helpers/notification';
class FileStore {
  folderData: Folder | {} = {};
  currentPath: Folders = [];
  isLoading: boolean = false;
  search: string = "";

  constructor() {
    makeAutoObservable(this);
  }

  resetStore = () => {
    runInAction(() => {
      this.folderData = {};
      this.currentPath = [];
      this.search = "";
      this.isLoading = false;
    });
  };
  handleApiError = async (error: any) => {
    if (error.response && error.response.status === 401) {
      try {
        const { token } = await UserStore.refreshToken();
        error.config.headers.Authorization = `Bearer ${token}`;
        await this.retryApiRequest(error.config);
        await this.getFiles();
      } catch (refreshError) {
        if (refreshError.response.status === 401) {
          UserStore.resetStore();
        }
        openNotification(refreshError.response.data.message)
      }
    } else {
      openNotification(error.response.data.message)
    }
  };

  retryApiRequest = async (config: any) => {
    try {
      const response = await api.request(config);
      return response.data;
    } catch (retryError) {
      openNotification(retryError.response.data.message)
      throw retryError;
    }
  };

  navigateToSubfolder = (folderData: Folder) => {
    const index = this.currentPath.findIndex((path) => path._id === folderData._id);
    if (index !== -1) {
      this.currentPath = this.currentPath.slice(0, index + 1);
    } else {
      this.currentPath.push(folderData);
    }
  };

  addFolder = async (folderData: CreateFolder) => {
    try {
      this.isLoading = true;
      const { data } = await api.post(`/folders/`, folderData);
      runInAction(() => {
        this.folderData = {
          ...(this.folderData as Folder),
          folders: [...(this.folderData as Folder).folders, data],
        };
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };

  editFolder = async ({ _id, ...folderData }) => {
    try {
      this.isLoading = true;

      const { data } = await api.patch<Folder>(`/folders/${_id}`, folderData);

      runInAction(() => {
        const updatedFolder = data as Folder;

        this.folderData = {
          ...(this.folderData as Folder),
          folders: this.updateItemInArray((this.folderData as Folder).folders, updatedFolder),
          ...(this.folderData.permissiveFolders && {
            permissiveFolders: this.updateItemInArray(this.folderData.permissiveFolders, updatedFolder),
          }),
        };
        this.isLoading = false;
      });
    } catch (error) {
      console.error("Error occurred while editing folder:", error);
      await this.handleApiError(error);
      this.isLoading = false;
    }
  };

  updateItemInArray = <T extends { _id: string }>(items: T[], updatedItem: T): T[] => {
    return items.map((item) => (item._id === updatedItem._id ? updatedItem : item));
  };
  editFolderPermissions = async (_id: string, permissions: Permissions) => {
    try {
      this.isLoading = true;
      const { data } = await api.patch<Folder>(`/folders/${_id}/permissions`, permissions);
      runInAction(() => {
        this.folderData = {
          ...(this.folderData as Folder),
          folders: this.updatePermissionsInArray((this.folderData as Folder).folders, data),
        };
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
      this.isLoading = false;
    }
  };
  updatePermissionsInArray = <T extends { _id: string }>(items: T[], updatedItem: T): T[] => {
    return items.map((item) =>
      item._id === updatedItem._id ? { ...item, permissions: updatedItem.permissions } : item
    );
  };
  deleteFolder = async (folderId: string) => {
    try {
      this.isLoading = true;
      await api.delete(`/folders/${folderId}`);
      runInAction(() => {
        const newFolders = (this.folderData as Folder).folders.filter((folder) => folder._id !== folderId);
        (this.folderData as Folder).folders = newFolders;
        UserStore.currentUser();
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };
  addFile = async (fileData: CreateFile) => {
    try {
      this.isLoading = true;
      const { data } = await api.post(`/files/`, fileData);
      runInAction(() => {
        (UserStore.userData as User).availableMemory = UserStore.userData.availableMemory - data.size;
        (this.folderData as Folder).files.push(data);
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };
  editFile = async (_id: string, fileData: File) => {
    try {
      this.isLoading = true;
      const { data } = await api.patch(`/files/${_id}`, fileData);
      runInAction(() => {
        const newFiles = this.folderData.files.map((file: File) => {
          if (file._id === data._id) {
            file = data;
          }
          return file;
        });
        this.folderData.files = newFiles;
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };
  editFilePermissions = async (_id: string, permissions: Permissions) => {
    try {
      this.isLoading = true;
      const { data } = await api.patch<File>(`/files/${_id}/permissions`, { permissions });
      runInAction(() => {
        this.folderData = {
          ...(this.folderData as Folder),
          files: this.updatePermissionsInArray((this.folderData as Folder).files, data),
        };
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
      this.isLoading = false;
    }
  };
  deleteFile = async (fileId: string) => {
    try {
      this.isLoading = true;
      await api.delete(`/files/${fileId}`);
      runInAction(() => {
        const newFiles = (this.folderData as Folder).files.filter((file) => file._id !== fileId);
        (this.folderData as Folder).files = newFiles;
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };
  getFiles = async (folderId) => {
    try {
      this.isLoading = true;
      this.search = "";
      const id = folderId ? folderId : UserStore.userData.mainFolder;
      const { data } = await api.get(`/folders/${id}`);
      runInAction(() => {
        this.navigateToSubfolder(data);
        this.folderData = data;
        this.isLoading = false;
      });
    } catch (error) {
      await this.handleApiError(error);
    }
  };
  searchFiles = async ({ keyword }) => {
    try {
      this.isLoading = true;
      this.currentPath = [this.currentPath[0]];
      this.search = keyword;
      if (keyword) {
        const { data } = await api.get(`/files`, { params: { keyword } });
        runInAction(() => {
          this.folderData = data;
          this.isLoading = false;
        });
      } else {
        runInAction(() => {
          this.getFiles(this.currentPath[0]._id);
          this.isLoading = false;
        });
      }
    } catch (error) {
      await this.handleApiError(error);
    }
  };
}

export default new FileStore();
