import fileStore from "./fileStore";
import userStore from "./userStore";

class RootStore {
    user = userStore;
    file = fileStore;
}

export default RootStore;