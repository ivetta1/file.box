import { makeAutoObservable, runInAction } from "mobx";
import { AuthFormInputs, ChangePasswordInputs, Product, User } from "../types/types";
import { api, setAxiosToken, unsetAxiosToken } from "../services/axios";
import {openNotification} from '../helpers/notification';
class UserStore {
  userData: User | {} = {};
  products: Product | [] = [];
  isAuth: boolean = false;
  isLoading: boolean = false;

  constructor() {
    makeAutoObservable(this);
    this.loadUserFromLocalStorage();
  }

  loadUserFromLocalStorage = async (): Promise<void> => {
    runInAction(() => {
      this.isLoading = true;
    });
    const token: string | null = localStorage.getItem("token");
    if (token) {
      setAxiosToken(token);
      try {
        await this.currentUser();
        runInAction(() => {
          this.isAuth = true;
        });
      } catch (error) {
        openNotification(error.response.data.message)
        this.resetStore();
      }
    } else {
      this.resetStore();
    }
    runInAction(() => {
      this.isLoading = false;
    });
  };

  saveTokenToLocalStorage = (data: User): void => {
    localStorage.setItem("token", data.token);
    localStorage.setItem("refreshToken", data.refreshToken);
  };

  resetStore = (): void => {
    runInAction(() => {
      this.userData = {};
      this.isAuth = false;
      this.isLoading = false;
      this.products = [];
      localStorage.removeItem("token");
      localStorage.removeItem("refreshToken");
    });
  };
  retryApiRequest = async (config: any) => {
    try {
      const response = await api.request(config);
      return response.data;
    } catch (retryError) {
      openNotification(retryError.response.data.message)
      throw retryError;
    }
  };

  handleApiError = async (error: any) => {
    if (error.response && error.response.status === 401) {
      try {
        const { token } = await this.refreshToken();
        error.config.headers.Authorization = `Bearer ${token}`;
        await this.retryApiRequest(error.config);
      } catch (refreshError) {
        if (refreshError.response.status === 401) {
          this.resetStore();
        }
        openNotification(refreshError.response.data.message)
      }
    } else {
      openNotification(error.response.data.message)
      
    }
  };


  signUp = async (payload: AuthFormInputs): Promise<void> => {
    try {
      this.isLoading = true;
      const { data } = await api.post<User>(`/users/signup`, payload);
      runInAction(() => {
        setAxiosToken(data.token);
        this.saveTokenToLocalStorage(data);
        this.userData = data;
        this.isAuth = true;
        this.isLoading = false;
      });
    } catch (error) {
      openNotification(error.response.data.message)
    }
  };

  signIn = async (payload: AuthFormInputs) => {
    try {
      this.isLoading = true;
      const { data } = await api.post<User>(`/users/signin`, payload);
      runInAction(() => {
        setAxiosToken(data.token);
        this.saveTokenToLocalStorage(data);
        this.userData = data;
        this.isAuth = true;
        this.isLoading = false;
      });
    } catch (error) {
      openNotification(error.response.data.message);
    }
  };

  currentUser = async () => {
    try {
      this.isLoading = true;
      const { data } = await api.get(`/users/whoami`);
      runInAction(() => {
        this.saveTokenToLocalStorage(data);
        this.userData = data;
        this.isAuth = true;
        this.isLoading = false;
      });
    } catch (error: any) {
      await this.handleApiError(error);
    }
  };
  buyMemory = async (productId) => {
    try {
      const { data } = await api.post(`/products/buy/${productId}`);
      window.location.href = data.url;
      runInAction(() => {
        this.currentUser();
      });
    } catch (error: any) {
      await this.handleApiError(error);
    }
  };
  getProducts = async () => {
    try {
      const { data } = await api.get(`/products`);
      runInAction(() => {
        this.products = data;
      });
    } catch (error: any) {
      await this.handleApiError(error);
    }
  };
  changePassword = async (passwords: ChangePasswordInputs) => {
    try {
      await api.patch(`/users/edit/password`, passwords);
    } catch (error: any) {
      await this.handleApiError(error);
    }
  };
  addToken = async (token: string) => {
    localStorage.setItem("token", token);
    setAxiosToken(token);
    await this.currentUser();
  };
  refreshToken = async () => {
    try {
      const refreshToken = localStorage.getItem("refreshToken");
      if (!refreshToken) throw new Error("No refreshToken found");
      setAxiosToken(refreshToken);
      const { data } = await api.get(`/users/refresh`);
      setAxiosToken(data.token);
      this.saveTokenToLocalStorage(data);
      return data;
    } catch (error) {
      this.resetStore();
      throw error;
    }
  };

  signOut = async () => {
    try {
      this.isLoading = true;
      await api.post(`/users/signout`);
      runInAction(() => {
        unsetAxiosToken();
        this.resetStore();
        this.isLoading = false;
      });
    } catch (error) {
      this.resetStore();
      this.isLoading = false;
    }
  };
}

const userStore = new UserStore();

export default userStore;
