import React, { lazy } from "react";
import { Route, Routes } from "react-router-dom";

import PrivateRoute from "./pages/routes/PrivateRoute";
import PublicRoute from "./pages/routes/PublicRoute";
import Layout from "./components/Layout/Layout";

const WelcomePage = lazy(() => import("./pages/WelcomePage"));
const AuthPage = lazy(() => import("./pages/AuthPage"));
const FilesPage = lazy(() => import("./pages/FilesPage"));

const App: React.FC = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Layout />}>
          <Route path="/" element={<PublicRoute redirectTo="/files" component={<WelcomePage />} />} />
          <Route path="/google/:token" element={<PublicRoute redirectTo="/files" component={<WelcomePage />} />} />
          <Route path="/auth/:type" element={<PublicRoute redirectTo="/files" component={<AuthPage />} />} />
          <Route path="/files" element={<PrivateRoute redirectTo="/" component={<FilesPage />} />} />
        </Route>
      </Routes>
    </>
  );
};

export default App;
