import { NavLink } from "react-router-dom";
import styled from "styled-components";

export const PublicPageWrapper = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
  weight: 100vw;
`;

export const AuthBox = styled.div`
  background-color:  ${({ theme: { colors } }) => colors.blackColor};
  border-radius: 8px;
  padding: 30px;
  width:70%;
    @media only screen and (min-width: 768px) {
   width:45%;
     padding: 35px;
  }
  @media only screen and (min-width: 1440px) {
   width:30%;
     padding: 40px;
  }
`;
export const AuthNav = styled.nav`
display:flex;
align-items: center;
margin-bottom: 10px;
    @media only screen and (min-width: 768px) {
  margin-bottom: 15px;
  }
  @media only screen and (min-width: 1440px) {
  margin-bottom: 20px;
  }`

export const SLink = styled(NavLink)`
  padding: 7px 0;
  margin-right: 25px;
  display: block;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
  color: ${({ theme: { colors } }) => colors.whiteColor};
  transition:  ${({ theme: { transition } }) => transition};
      transition:${({ theme: { transition } }) => `color ${transition}`};
&.active {
    color: ${({ theme: { colors } }) => colors.accentColor};
}
  &:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
`;
export const SGoogle = styled.a`
color: ${({ theme: { colors } }) => colors.whiteColor};
font-size: ${({ theme: { fontSizes } }) => fontSizes.big};
margin-left: auto;
  &:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
   `