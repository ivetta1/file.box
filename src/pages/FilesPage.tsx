import { useEffect, useState, useMemo, useCallback } from "react";
import { observer } from "mobx-react-lite";
import { PlusOutlined, FileOutlined, FolderOpenOutlined } from "@ant-design/icons";
import {
  Header,
  Title,
  Buttons,
  Button,
  ButtonName,
  Path,
  FolderPath,
  Memory,
  Tools,
  BtnMenu,
  MemoryBar,
} from "./FilesPage.styled";
import FolderFormModal from "../components/Modals/FolderFormModal";
import FolderDeleteModal from "../components/Modals/FolderDeleteModal";
import FileFormModal from "../components/Modals/FileFormModal";
import FileDeleteModal from "../components/Modals/FileDeleteModal";
import ChangePermissionsModal from "../components/Modals/ChangePermissionsModal";
import FoldersList from "../components/Files/FoldersList/FoldersList";
import FilesList from "../components/Files/FilesList/FilesList";
import SearchForm from "../components/Forms/SearchForm";
import { useStores } from "../context/rootStoreContext";
import { File, Folder } from "../types/types";
import { Dropdown } from "antd";
import { MenuProps } from "react-select";

const FilesPage: React.FC = observer(() => {
  const { user, file } = useStores();
  const { userData, getProducts, products = [], buyMemory } = user;
  const { getFiles, currentPath, resetStore } = file;

  const rootFolder = currentPath[currentPath.length - 1];

  const [currentFolder, setCurrentFolder] = useState<Folder | {} | null>(null);
  const [folderForDelete, setFolderForDelete] = useState<Folder | null>(null);
  const [currentFile, setCurrentFile] = useState<File | {} | null>(null);
  const [fileForDelete, setFileForDelete] = useState<File | null>(null);
  const [changeFilePermissions, setChangeFilePermissions] = useState<File | Folder | null>(null);

  const handleCloseModal = useCallback((modal: string) => {
    if (modal === "addFolder") {
      setCurrentFolder(null);
    } else if (modal === "deleteFolder") {
      setFolderForDelete(null);
    } else if (modal === "addFile") {
      setCurrentFile(null);
    } else if (modal === "deleteFile") {
      setFileForDelete(null);
    } else if (modal === "changePermissions") {
      setChangeFilePermissions(null);
    }
  }, []);

  useEffect(() => {
    if (userData.mainFolder) {
      getFiles(userData.mainFolder);
    }
    return () => {
      resetStore();
    };
  }, [userData]);

  const modalState = useMemo(
    () => ({
      addFolder: !!currentFolder,
      deleteFolder: !!folderForDelete,
      addFile: !!currentFile,
      deleteFile: !!fileForDelete,
      changePermissions: !!changeFilePermissions,
    }),
    [currentFolder, folderForDelete, currentFile, fileForDelete, changeFilePermissions]
  );
  const items: MenuProps["items"] = products.map((product) => {
    return {
      key: product._id,
      label: (
        <BtnMenu type="button" onClick={() => buyMemory(product._id)}>
          {product.name} - {product.price}
          {product.currency}
        </BtnMenu>
      ),
    };
  });
  return (
    <>
      <div>
        <Header>
          <Title>Files</Title>
          <MemoryBar>
            <Memory $amount={userData.availableMemory}>Free {Math.round(userData.availableMemory / 1024000)}Mb</Memory>

            <Dropdown menu={{ items }} placement="bottomLeft" overlayClassName="custom-dropdown-menu">
              <Button type="button" onClick={getProducts}>
                <PlusOutlined />
                <ButtonName>Buy</ButtonName>
              </Button>
            </Dropdown>
          </MemoryBar>
          {!rootFolder ||
            ((rootFolder.owner === userData._id ||
              !!rootFolder.permissions.find(
                (permit) => permit.email === userData.email && permit.role === "editor"
              )) && (
              <Buttons>
                <Button type="button" onClick={() => setCurrentFile({})}>
                  <PlusOutlined />
                  <FileOutlined />
                  <ButtonName>Add file</ButtonName>
                </Button>
                <Button type="button" onClick={() => setCurrentFolder({})}>
                  <PlusOutlined />
                  <FolderOpenOutlined />
                  <ButtonName>Add folder</ButtonName>
                </Button>
              </Buttons>
            ))}
        </Header>
        <Tools>
          <Path>
            {currentPath.length > 0 &&
              currentPath.map((path, index) => (
                <FolderPath
                  key={path._id}
                  onClick={() => getFiles(path._id)}
                  disabled={currentPath.length === index + 1}
                >
                  {index !== 0 ? path.title : "Root folder"} <span>/</span>
                </FolderPath>
              ))}
          </Path>
          <SearchForm />
        </Tools>
        <FoldersList
          setFolderForEdit={setCurrentFolder}
          setFolderForDelete={setFolderForDelete}
          setChangeFilePermissions={setChangeFilePermissions}
        />
        <FilesList
          setFileForEdit={setCurrentFile}
          setFileForDelete={setFileForDelete}
          setChangeFilePermissions={setChangeFilePermissions}
        />
      </div>

      {currentFolder && (
        <FolderFormModal
          isOpen={modalState.addFolder}
          onClose={() => handleCloseModal("addFolder")}
          folder={currentFolder}
        />
      )}
      {folderForDelete && (
        <FolderDeleteModal
          isOpen={modalState.deleteFolder}
          onClose={() => handleCloseModal("deleteFolder")}
          folder={folderForDelete}
        />
      )}
      {currentFile && (
        <FileFormModal isOpen={modalState.addFile} onClose={() => handleCloseModal("addFile")} file={currentFile} />
      )}
      {fileForDelete && (
        <FileDeleteModal
          isOpen={modalState.deleteFile}
          onClose={() => handleCloseModal("deleteFile")}
          file={fileForDelete}
        />
      )}
      {changeFilePermissions && (
        <ChangePermissionsModal
          isOpen={modalState.changePermissions}
          onClose={() => handleCloseModal("changePermissions")}
          file={changeFilePermissions}
        />
      )}
    </>
  );
});

export default FilesPage;
