import { Navigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useStores } from "../../context/rootStoreContext";
interface PrivateRouteProps {
  component?: React.ComponentType<any>;
  redirectTo?: string;
}

const PrivateRoute: React.FC<PrivateRouteProps> = observer(({ component: Component, redirectTo = "/" }) => {
  const {
    user: { isAuth },
  } = useStores();
  return isAuth ? Component : <Navigate to={redirectTo} />;
});

export default PrivateRoute;
