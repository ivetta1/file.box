import { Navigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useStores } from "../../context/rootStoreContext";

interface PublicRouteProps extends React.PropsWithChildren {
  component?: React.ComponentType<any>;
  redirectTo?: string;
}

const PublicRoute: React.FC<PublicRouteProps> = observer(({ component: Component, redirectTo = "/files" }) => {
  const {
    user: { isAuth },
  } = useStores();
  return isAuth ? <Navigate to={redirectTo} /> : Component;
});

export default PublicRoute;
