import { AuthForm } from "../components/Forms/AuthForm";
import { GoogleOutlined } from "@ant-design/icons";
import { PublicPageWrapper, AuthBox, AuthNav, SLink, SGoogle } from "./AuthPage.styled";
const { VITE_API_BASE_URL_LOCAL } = import.meta.env;

const AuthPage: React.FC = () => {

  return (
    <PublicPageWrapper>
      <AuthBox>
        <AuthNav>
          <SLink to="/auth/signup" aria-label="Link to sign up">
            Sign Up
          </SLink>
          <SLink to="/auth/signin" aria-label="Link to sign in">
            Sign In
          </SLink>

          <SGoogle href={`${VITE_API_BASE_URL_LOCAL}/users/google`}>
            <GoogleOutlined />
          </SGoogle>
        </AuthNav>
        <AuthForm />
      </AuthBox>
    </PublicPageWrapper>
  );
};

export default AuthPage;
