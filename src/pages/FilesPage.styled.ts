import styled from "styled-components";

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 50px;
`;
export const Title = styled.div`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.big};
  font-weight: 600;
  margin: 0;
`;
export const MemoryBar = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
`;
export const Memory = styled.div`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.sm};
  color: ${({ theme: { colors }, $amount }) => ($amount > 1000 ? colors.blackColor : colors.errorColor)};
  font-weight: 500;
  margin: 0;
  text-align: center;
  @media only screen and (min-width: 768px) {
    font-size: ${({ theme: { fontSizes } }) => fontSizes.ml};
  }
`;
export const Buttons = styled.div`
  display: flex;
  align-items: center;
`;
export const Button = styled.button`
  padding: 7px;
  border-radius: 8px;
  border: none;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  background-color: ${({ theme: { colors } }) => colors.blackColor};
  margin-left: 5px;
  display: flex;
  justify-content: space-between;
  &:hover {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
  & span {
    margin: 0 5px;
  }
  @media only screen and (min-width: 768px) {
    margin-left: 10px;
  }
`;

export const ButtonName = styled.span`
  display: none;
  @media only screen and (min-width: 768px) {
    margin-left: 10px;
    display: block;
  }
`;
export const Tools = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 30px;
`;

export const Path = styled.div`
  display: flex;
`;

export const FolderPath = styled.button`
  border: none;
  background-color: ${({ theme: { colors } }) => colors.transparent};
`;
export const BtnMenu = styled.button`
  border: none;
  color: ${({ theme: { colors } }) => colors.whiteColor};
  background: ${({ theme: { colors } }) => colors.transparent};
  &:hover,
  &:focus {
    color: ${({ theme: { colors } }) => colors.accentColor};
  }
`;
