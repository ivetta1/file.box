import React from "react";
import FileBoxLogo from "../assets/logo.webp";
import { PublicPageWrapper, MainTitle, SImg, SNav, SLink, SAccentLink, Subtitle } from "./WelcomePage.styled";
import { useParams } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useStores } from "../context/rootStoreContext";

const WelcomePage: React.FC = observer(() => {
  const {
    user: { addToken },
  } = useStores();
  const { token } = useParams();
  if (token && token !== "error") {
    addToken(token);
  }
  return (
    <PublicPageWrapper>
      <SImg src={FileBoxLogo} alt="File.box logo" />
      <MainTitle>File.box App</MainTitle>
      <Subtitle>
        Introducing File.box App: Your all-in-one file storage, management, and collaboration app. Seamlessly sync files
        across users, manage permissions, and share access via email. Simplify your workflow with File.box App today.
      </Subtitle>
      <SNav>
        <ul>
          <li>
            <SAccentLink to="/auth/signup" aria-label="Link to sign up">
              Sign Up
            </SAccentLink>
          </li>
          <li>
            <SLink to="/auth/signin" aria-label="Link to sign in">
              Sign In
            </SLink>
          </li>
        </ul>
      </SNav>
    </PublicPageWrapper>
  );
});

export default WelcomePage;
