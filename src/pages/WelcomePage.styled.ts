import { Link } from "react-router-dom";
import styled from "styled-components";

export const PublicPageWrapper = styled.main`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
  weight: 100vw;
  max-width: 80%;
  margin: 0 auto;
  @media only screen and (min-width: 768px) {
    max-width: 60%;
  }
  @media only screen and (min-width: 1440px) {
    max-width: 50%;
  }
`;

export const SImg = styled.img`
  width: 50%;
  @media only screen and (min-width: 768px) {
    width: 40%;
  }
  @media only screen and (min-width: 1440px) {
    width: 30%;
  }
`;
export const MainTitle = styled.h1`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.gigantic};
  margin: 35px 0;
`;
export const Subtitle = styled.h3`
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
  margin-bottom: 35px;
  font-weight: 300;
  text-align: center;
  @media only screen and (min-width: 768px) {
    margin-bottom: 45px;
  }
  @media only screen and (min-width: 1440px) {
    margin-bottom: 55px;
  }
`;

export const SNav = styled.nav`
  width: 100%;
  & li {
    width: 100%;
  }
`;

export const SAccentLink = styled(Link)`
  width: 50%;
  text-align: center;
  padding: 14px 0;
  border-radius: 8px;
  display: block;
  margin: 0 auto;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
  background-color: ${({ theme: { colors } }) => colors.blackColor};
  color: ${({ theme: { colors } }) => colors.whiteColor};
  border: ${({ theme: { colors } }) => `1px solid ${colors.blackColor}`};
 
  transition:${({ theme: { transition } }) => `background-color ${transition}, color ${transition}, border-color ${transition}`};
  margin-bottom: 15px;
  &:hover {
    color: ${({ theme: { colors } }) => colors.blackColor};
    background-color: ${({ theme: { colors } }) => colors.transparent};
  }
`;

export const SLink = styled(Link)`
  width: 50%;
  text-align: center;
  padding: 14px 0;
  border-radius: 8px;
  display: block;
  margin: 0 auto;
  font-size: ${({ theme: { fontSizes } }) => fontSizes.xl};
  color: ${({ theme: { colors } }) => colors.blackColor};
  background-color: ${({ theme: { colors } }) => colors.transparent};
    transition:${({ theme: { transition } }) => `background-color ${transition}, color ${transition}, border-color ${transition}`};
  &:hover {
    color: ${({ theme: { colors } }) => colors.whiteColor};
  }
`;
