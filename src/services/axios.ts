import axios from 'axios';

const BASE_URL = import.meta.env.VITE_API_BASE_URL_PROD;

export const api = axios.create({
    baseURL: BASE_URL
});

export const setAxiosToken = (token: string) => {
    api.defaults.headers.common.Authorization = `Bearer ${token}`;
};

export const unsetAxiosToken = () => {
    api.defaults.headers.common.Authorization = '';
};