export type ChangePasswordInputs = {
  oldPassword: string;
  newPassword: string;
};
export type AuthFormInputs = {
  email: string;
  password: string;
};

export interface CreateFolder {
  title: string;
  rootFolder: string;
}
export interface Folder extends CreateFolder {
  _id: string;
  isPrivate: boolean;
  owner: string;
  title: string;
  files: Files;
  folders: Folders;
  createdAt: string;
  updatedAt: string;
  permissions: Permit[];
}
export type Folders = Folder[] | [];

export interface Search {
  keyword: string;
}

export interface CreateFile {
  name: string;
  size: number;
  url: string;
  isPrivate: boolean;
}

export interface File extends CreateFile {
  _id: string;
  isPrivate: boolean;
  name: string;
  owner: string;
  rootFolder: string;
  size: number;
  url: string;
  createdAt: string;
  updatedAt: string;
  permissions: Permit[];
}
export type Files = File[] | [];

export type Product = {
    _id: string,
    stripeID: string;
    name: string;
    price: number;
    currency: string;
    value: number
}
export interface User {
  _id: string;
  name: string;
  email: string;
  availableMemory: number;
  mainFolder: string;
  token: string;
  refreshToken: string;
  createdAt: string;
  updatedAt: string;
}

export interface ModalProps {
  isOpen: boolean;
  onClose: () => void;
  folder?: Folder | {} | null;
  file?: File | {} | null;
}
export interface Permit {
  email: string;
  role: "viewer" | "editor";
}

export type Permissions = Permit[] | [];
