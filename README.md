# [File.box](https://file.box.ivettadashkova.com/)

### is the client-side application responsible for the user interface and interaction with the FileBox server. It's built using Vite, React.js, and TypeScript, following a component-based architecture. This frontend allows users to upload, manage, and share files and folders stored on the server.

## Technologies Used

- **Vite:** A build tool that aims to provide a faster and leaner development experience for modern web projects.
- **React.js + TypeScript:** Frontend library for building user interfaces with static typing.
- **Component-based Architecture:** Organizing the UI into reusable components for better maintainability.
- **State Management:** Utilizing MobX for state management.
- **Routing:** Using React Router DOM for navigation.
- **UI Components:** Ant Design for ready-to-use UI components.
- **Form Handling:** React Hook Form for form management.
- **Styling:** Styled Components for styling components with CSS.
- **File Upload:** React Dropzone for drag-and-drop file upload functionality.
- **Other Libraries:** Axios for handling HTTP requests, React Loader Spinner for loading indicators, React Select for custom select components, etc.

## Features

- **User Authentication:** Login and authentication via Google.
- **File and Folder Management:** Create, rename, delete, and move files and folders.
- **Permissions Management:** Set files and folders as public or private, and share access via email with specific permissions.
- **File Upload:** Upload files to the server with ease.
- **Search Functionality:** Search for files and folders by name.
- **Responsive UI:** Ensure a seamless experience across different devices and screen sizes.
- **User-friendly Interface:** Intuitive and easy-to-use interface for smooth navigation and interaction.

## Backend and API Documentation

- **Backend URL:** [https://file-box-server.onrender.com](https://file-box-server.onrender.com)
- **API Documentation:** [https://file-box-server.onrender.com/api](https://file-box-server.onrender.com/api)
- **Repository:** [https://gitlab.com/ivettafsd/file.box-server](https://gitlab.com/ivettafsd/file.box-server)

## Running the Application

1. Clone the repository:

```bash
git clone https://gitlab.com/ivettafsd/file.box.git
cd file.box
```

2. Install dependencies:

```bash
npm install
```

3. Set up environment variables:

Create a `.env` file in the root directory of the frontend project and add the following environment variables:

```plaintext
VITE_API_BASE_URL_PROD=https://file-box-server.onrender.com
```

4. Start the development server:

```bash
npm run dev
```
